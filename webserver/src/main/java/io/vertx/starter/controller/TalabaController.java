package io.vertx.starter.controller;

import io.vertx.ext.web.RoutingContext;

public class TalabaController {

    public void getTalabalar(RoutingContext context) {
        context.response().end("Tabalar ro`yxati");
    }

    public void yigindi(RoutingContext context) {
        String son1 = context.request().params().get("son1");
        String son2 = context.request().params().get("son2");


        context.response().end("Yig`indi " + son1 + " + " + son2 + " = ?");
    }
}
